<?php

   class Image extends Controller{

      public function __construct(){
         parent::__construct();
      }

      public function doUpload(){
         $filename = $_FILES['image'];
         $image = $this->upload->doUpload($filename, true);
         $this->model->save($image);
         $this->view->render('image/success');
      }

      public function index($id=''){
         $this->view->id = $id;
         $this->view->render('image/index');
      }

      public function upload($id){
         $this->view->id = $id;
         $this->view->render('image/upload');
      }
   }
?>
